




;***************************************************************************************************
; FILE: test18a.s    Test BSUB @somelabel where somelabel is both BEFORE and after the BLT instruction.

;***************************************************************************************************

;---------------------------------------------------------------------------------------------------
; Defines a data segment. The data segment is located at memory address 271 =00010Fh.
;---------------------------------------------------------------------------------------------------

.DATA 271


;---------------------------------------------------------------------------------------------------
; Defines a text (code) segment. The text segment is located at memory address 1024 = 00400h.
;---------------------------------------------------------------------------------------------------

.TEXT 1024

@loop	   LDI %A -12
	   LDI %B 2
	   BSUB @loop
	   HALT       ; This would be an infinite loop, ha

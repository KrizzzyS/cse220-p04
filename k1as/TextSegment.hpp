//**************************************************************************************************************
// CLASS: cTextSegment
//
// DESCRIPTION
// Maintains the text segment. The text segment has two data members: a Dictionary (or map) of the labels, and
// a Vector of the instructions. cTextSegment is derived from cSegment.
//
// AUTHOR INFORMATION
// Kevin R. Burger [KRB]
//
// Mailing Address:
// Computer Science & Engineering
// School of Computing, Informatics, and Decision Systems Engineering
// Arizona State University
// Tempe, AZ 85287-8809
//
// Email: burgerk@asu
// Web:	  http://kevin.floorsoup.com
//**************************************************************************************************************
#ifndef TEXTSEGMENT_HPP
#define TEXTSEGMENT_HPP

#include <vector> // Added this include directive for vector structure in the private data members. Reminder.
using std::vector;

#include <map>
using std::map;

#include <string>
using std::string;

#include "Segment.hpp"
#include "Types.hpp"
#include "Instr.hpp"
#include "Label.hpp"

class cTextSegment : public cSegment {
    
public:
	//==========================================================================================================
	// PUBLIC FUNCTION MEMBERS
	//==========================================================================================================
    
	//----------------------------------------------------------------------------------------------------------
	// Default ctor.
	//----------------------------------------------------------------------------------------------------------
	cTextSegment(tAddress const pAddress = 0);
    
	//----------------------------------------------------------------------------------------------------------
	// Copy ctor.
	//----------------------------------------------------------------------------------------------------------
	cTextSegment(cTextSegment const& pTextSegment);
    
	//----------------------------------------------------------------------------------------------------------
	// Dtor.
	//
	// REMARK
	// Note that the base class destructor will be called automatically, AFTER the body of this dtor is executed
	//
	// REMARK
	// I inline functions that do nothing.
	//----------------------------------------------------------------------------------------------------------
	~cTextSegment()
	{
	}
    
	//----------------------------------------------------------------------------------------------------------
	// Add()
	//
	// DESCRIPTION
	// Adds pInstr to the std::map mInstrs. Didn't include const in the parameter, like Burger. Reminder. 
	//----------------------------------------------------------------------------------------------------------
	void Add(cInstr& pInstr);
    
    //----------------------------------------------------------------------------------------------------------
	// Add()
	//
	// DESCRIPTION
	// Adds pLabel to the std::map mLabels. 
	//----------------------------------------------------------------------------------------------------------
	void Add(cLabel const& pLabel);
    
    
	//----------------------------------------------------------------------------------------------------------
	// Contents()
	//
	// REMARK
	// "tByte const *" states that this function returns a pointer to a constant tByte (or tBytes). In English,
	// this means that the "constant tBytes" to which this pointer points cannot be modified via the pointer.
	// They can be modified via a different pointer, but not by this one.
	//----------------------------------------------------------------------------------------------------------
	tByte const *Contents() const;
    
	//----------------------------------------------------------------------------------------------------------
	// Label()
	//
	// DESCRIPTION
	// Retrieves the Label with name pName from the std::map mLabels.
	//
	// REMARK
	// This is essentially an accessor function for the mLabels data member, so I inlined it.
	//----------------------------------------------------------------------------------------------------------
	cLabel Label(string const& pName)
	{
		return mLabels[pName];
	}
	
    //----------------------------------------------------------------------------------------------------------
	// Size()
	//----------------------------------------------------------------------------------------------------------
	tUint32 Size() const;
    
	//----------------------------------------------------------------------------------------------------------
	// operator=()
	//----------------------------------------------------------------------------------------------------------
	cTextSegment& operator=(cTextSegment const& pTextSegment);
    
protected:
	//==========================================================================================================
	// PROTECTED FUNCTION MEMBERS
	//==========================================================================================================
    
	//----------------------------------------------------------------------------------------------------------
	// Copy() 
	//----------------------------------------------------------------------------------------------------------
	void Copy(cTextSegment const& pTextSegment);
    
private:
	//==========================================================================================================
	// PRIVATE DATA MEMBERS
	//==========================================================================================================
    
	// The cVariables in the data segment are stored in a map. "map" is a class in the C++ Standard Template
	// Library (STL). A map is also called a "dictionary". A map maps "keys" onto "values". In our map, named
	// mVars, the key is string and the value is a cVariable.
    
    vector<cInstr> mInstrs;
	map<string, cLabel> mLabels;
};

#endif
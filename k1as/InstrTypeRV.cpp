//**************************************************************************************************************
// CLASS: cInstrTypeRV
//
// DESCRIPTION
// See comments in InstrTypeRV.hpp.
//
// AUTHOR INFORMATION
// Kevin R. Burger [KRB]
//
// Mailing Address:
// Computer Science & Engineering
// School of Computing, Informatics, and Decision Systems Engineering
// Arizona State University
// Tempe, AZ 85287-8809
//
// Email: burgerk@asu
// Web:	  http://kevin.floorsoup.com
//**************************************************************************************************************
#include "InstrTypeRV.hpp"

//--------------------------------------------------------------------------------------------------------------
// Default ctor. FILLED
//
// PSEUDOCODE
// 
//--------------------------------------------------------------------------------------------------------------
cInstrTypeRV::cInstrTypeRV() :
    mVariable() // *** Not sure about zeros. Reminder. *** used to have mVariable("", 0, 0)
{               // *** changed compared with Brian's mVariable() ***
}

//--------------------------------------------------------------------------------------------------------------
// Another ctor. FILLED
//
// PSEUDOCODE
//
//--------------------------------------------------------------------------------------------------------------
cInstrTypeRV::cInstrTypeRV(string const& pMnemonic, cRegister const& pReg, cVariable const& pVar) :
    cInstrTypeR(pMnemonic, pReg),
    mVariable(pVar)
{
}

//--------------------------------------------------------------------------------------------------------------
// Copy ctor. FILLED 
//
// PSEUDOCODE
// 
//--------------------------------------------------------------------------------------------------------------
cInstrTypeRV::cInstrTypeRV(cInstrTypeRV const& pInstr) :
    cInstrTypeR(pInstr),
    mVariable(pInstr.mVariable)
{
}
//--------------------------------------------------------------------------------------------------------------
// Copy() FILLED
//
// DESCRIPTION
// Makes this cInstrTypeRV a copy of pInstr.
//
// PSEUDOCODE
//
//--------------------------------------------------------------------------------------------------------------
void cInstrTypeRV::Copy(cInstrTypeRV const& pInstr)
{
    cInstrTypeRV::Copy(pInstr);
    mVariable = pInstr.mVariable;
    Encode();
}

//--------------------------------------------------------------------------------------------------------------
// Encode() FILLED (went through logic with appending the address) 
//
// DESCRIPTION:
// 
//
// PSEUDOCODE
// 
//--------------------------------------------------------------------------------------------------------------
void cInstrTypeRV::Encode()
{
    cInstrTypeR::Encode();
    Encoding(Encoding() | mVariable.Address());
}

//--------------------------------------------------------------------------------------------------------------
// operator=() FILLED
//--------------------------------------------------------------------------------------------------------------
cInstrTypeRV& cInstrTypeRV::operator=(cInstrTypeRV const& pInstr)
{
    if (this != &pInstr) Copy(pInstr);
    return *this;
}


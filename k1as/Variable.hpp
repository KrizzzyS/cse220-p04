//**************************************************************************************************************
// CLASS: cVariable
//
// DESCRIPTION
// Declares a class that represents variable operands in assembly language instructions. This class is derived
// from cOperand. The cOperand class stores the name of the cVariable as a string, and this class adds a tInt32
// data member which is the 2's complement initial value of the cVariable and a tAddress data member which is
// the address of the cVariable within the data segment.
//
// AUTHOR INFORMATION
// Kevin R. Burger [KRB]
//
// Mailing Address:
// Computer Science & Engineering
// School of Computing, Informatics, and Decision Systems Engineering
// Arizona State University
// Tempe, AZ 85287-8809
//
// Email: burgerk@asu
// Web:	  http://kevin.floorsoup.com
//**************************************************************************************************************
#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include <string>
using std::string;

#include "Operand.hpp"

//==============================================================================================================
// CLASS: Variable
//==============================================================================================================
class cVariable : public cOperand {
    
public:
	//==========================================================================================================
	// PUBLIC FUNCTION MEMBERS
	//==========================================================================================================
    
	//---------------------------------------------------------------------------------------------------------
	// Default ctor.
	//---------------------------------------------------------------------------------------------------------
	cVariable(string const& pName = "", tAddress const pAddress = 0, tWord const pInitValue = 0);
    
	//---------------------------------------------------------------------------------------------------------
	// Copy ctor.
	//---------------------------------------------------------------------------------------------------------
	cVariable(cVariable const& pVariable);
    
	//---------------------------------------------------------------------------------------------------------
	// Dtor.
	//---------------------------------------------------------------------------------------------------------
	~cVariable()
	{
	}
    
    //---------------------------------------------------------------------------------------------------------
	// Address()
	//
	// DESCRIPTION
	// Returns the address of a variable, inlined that beast.
	//---------------------------------------------------------------------------------------------------------
    
    tAddress Address() const
    {
        return mAddress;
    }
    
    //---------------------------------------------------------------------------------------------------------
	// Address()
	//
	// DESCRIPTION
	// Sets the address of a variable, inlined that beast. 
	//---------------------------------------------------------------------------------------------------------
	void Address(tAddress const pAddress)
    {
        mAddress = pAddress;
    }
    
    //---------------------------------------------------------------------------------------------------------
	// InitValue()
	//
	// DESCRIPTION
	// Returns the initial value of the variable 
	//---------------------------------------------------------------------------------------------------------
    
    tInt32 InitValue() const
    {
        return mInitValue;
    }
    
    //---------------------------------------------------------------------------------------------------------
	// InitValue()
	//
	// DESCRIPTION
	// Sets the initial value of the variable, inlined that beast. 
	//---------------------------------------------------------------------------------------------------------
	void InitValue(tInt32 const pInitValue)
    {
        mInitValue = pInitValue; 
    }
    
    //---------------------------------------------------------------------------------------------------------
	// Name()
	//
	// DESCRIPTION
	// Returns the name of the variable, i.e., either "$a" or "$sum", "$n", etc.
	//---------------------------------------------------------------------------------------------------------
	string Name() const
	{
		return String();
	}
    
	//---------------------------------------------------------------------------------------------------------
	// Name(string const&) // used Name function from Register.hpp (should be the same)
	//
	// DESCRIPTION
	// Sets the name of the variable.
	//---------------------------------------------------------------------------------------------------------
	void Name(string const& pName)
	{
		String(pName);
	}
    
	//---------------------------------------------------------------------------------------------------------
	// operator=()
	//---------------------------------------------------------------------------------------------------------
	cVariable& operator=(cVariable const& pVariable);
    
protected:
	//==========================================================================================================
	// PROTECTED FUNCTION MEMBERS
	//==========================================================================================================
    
	//---------------------------------------------------------------------------------------------------------
	// Copy()
	//---------------------------------------------------------------------------------------------------------
	void Copy(cVariable const& pVariable);

private:
    //==========================================================================================================
	// PRIVATE DATA MEMBERS
	//==========================================================================================================
    
	tAddress mAddress;
	tInt32 mInitValue;
};

#endif

